//
//  SimpleSwipeUIView.swift
//  SwipeButtonView
//
//  Created by Louise Chan on 2020-12-14.
//

import UIKit
import AVFoundation

class SimpleSwipeUIView: UIView {

    private var imageView = UIImageView()
    private var swipeLabel = UILabel()
    var isSwipeSoundActive = true
    private var swipeEndReached = false
    private var action: (()->())? = nil
    
    private var swipeButtonLeadingConstraint: NSLayoutConstraint?
        
    init() {
        
        super.init(frame: CGRect(x: 0, y: 0, width: 200, height: 50))
        
        // Initialize view holder's properties
        self.backgroundColor = .systemBackground
        self.layer.cornerRadius = 15
        self.clipsToBounds = true
        self.addSubview(imageView)
        self.addSubview(swipeLabel)
        
        // Initialize image view
        imageView.image = UIImage(systemName: "greaterthan.square.fill")
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        imageView.addGestureRecognizer(panGesture)
        
        // Initialize label
        swipeLabel.text = "Swipe to the right"
        swipeLabel.font = .systemFont(ofSize: 14)
        swipeLabel.textAlignment = .center
        swipeLabel.backgroundColor = .gray
        setupConstraints()
        
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setSwipeAction(action: @escaping ()->()) {
        self.action = action
    }
        
    private func setupConstraints() {
        imageView.translatesAutoresizingMaskIntoConstraints = false
        swipeLabel.translatesAutoresizingMaskIntoConstraints = false
        
        swipeButtonLeadingConstraint = imageView.leadingAnchor.constraint(equalTo: self.leadingAnchor)
        
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: self.topAnchor, constant: -2),
            imageView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 2),
            swipeButtonLeadingConstraint!,
            //imageView.heightAnchor.constraint(equalTo: self.heightAnchor),
            imageView.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.20),
            
            swipeLabel.topAnchor.constraint(equalTo: self.topAnchor),
            swipeLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            swipeLabel.leadingAnchor.constraint(equalTo: imageView.trailingAnchor),
            swipeLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor)
        ])
        
    }
    
    @objc func handlePanGesture(_ sender: UIPanGestureRecognizer) {
        
        let translation = sender.translation(in: self)
    
        guard let swipeButtonLeadingConstraint = swipeButtonLeadingConstraint else { return }
        
        if sender.state == .changed && translation.x >= 0 {
            if translation.x + imageView.frame.width < self.frame.width {
                swipeButtonLeadingConstraint.constant = translation.x
                self.layoutIfNeeded()
            } else {
                swipeButtonLeadingConstraint.constant = self.frame.width - imageView.frame.width - 1
                
                // create a sound ID, in this case its the tweet sound.
                
                if !swipeEndReached {
                    if isSwipeSoundActive {
                        // Play lock sound
                        let systemSoundID: SystemSoundID = 1305
                        AudioServicesPlaySystemSound (systemSoundID)
                    }
                    
                    // Indicate that end of swiped has been reached
                    swipeEndReached = true
                    
                    // Call action to perform once end of swipe is reached
                    guard let completionHandler = self.action else {return}
                    completionHandler()
                    
                }
            }
        } else if sender.state == .ended && swipeButtonLeadingConstraint.constant > 0 && !swipeEndReached {
            swipeButtonLeadingConstraint.constant = 0
            swipeEndReached = false
            UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 4, options: .curveEaseOut, animations: {
                self.layoutIfNeeded()
            }, completion: nil)
        }
    }
}
