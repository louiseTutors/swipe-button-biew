//
//  ViewController.swift
//  SwipeButtonView
//
//  Created by Louise Chan on 2020-12-14.
//

import UIKit

class ViewController: UIViewController {
    private var testSwipeButton = SimpleSwipeUIView()

    @IBOutlet private var contentView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        testSwipeButton.setSwipeAction {
            self.segueTo2ndScreen()
        }
        view.addSubview(testSwipeButton)
        
        
    }
    
    
    override func viewDidLayoutSubviews() {
        // Apply layout constraints for the swipe button view
        testSwipeButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            testSwipeButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            testSwipeButton.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -view.safeAreaLayoutGuide.layoutFrame.height * 0.10),
            testSwipeButton.heightAnchor.constraint(equalToConstant: view.safeAreaLayoutGuide.layoutFrame.height * 0.075),
            testSwipeButton.widthAnchor.constraint(equalToConstant: view.safeAreaLayoutGuide.layoutFrame.width * 0.7)
        ])

    }
    private func segueTo2ndScreen() {
        performSegue(withIdentifier: "toSecondScreen", sender: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        segue.destination.modalPresentationStyle = .fullScreen
    }
}

